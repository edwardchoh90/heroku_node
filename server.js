const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
const port = process.env.PORT || 3000;

let app = express();
let webRequest;
let maintenance = false;

//app.use middleware __dir name = directory name of the project

app.use((req,res,next)=>{

    let now = new Date().toString();

    webRequest = (`${now} : ${req.method} from ${req.url}`);
    fs.appendFileSync( 'server.log' ,webRequest + '\n' );
    next();


});

if (maintenance){
    app.use((req,res,next)=>{

        res.render('maintenance.hbs', {
            pageTitle: 'Maintenance',
            message: 'Be Right Back',
        })

    });

}


app.use(express.static(__dirname + '/public'));

hbs.registerPartials(__dirname +'/partials');

//global functions - registerHelper
//register partials - component

hbs.registerHelper('getCurrentYear',()=>{
    return new Date().getFullYear();
});

hbs.registerHelper('capitalize',(text)=>{
    return text.toUpperCase();
});

app.set('view engine', 'hbs');

//manually get request

app.get('/',(req,res) => {
    /*
    let data = {
        name:'Edward',
        likes:'Code'
    }
    */
    //res.send('<h1>Hello Express World</h1>');
    res.render('home.hbs',{
        pageTitle:'Home Page',
        message:'Hello & Welcome',
        data:webRequest
    })


});

app.get('/about',(req,res)=>{

   res.render('about.hbs',{
       pageTitle:'About Page',
   });


});

app.get('/bad',(req,res)=>{

    res.send({
        errorMessage:'error 404'
    })

});

app.get('/project',(req,res)=>{

    res.render('project.hbs',
        {   pageTitle:'Project Page',
            content:"this is project section"})

});



app.listen(port,()=>{
    console.log(`server is up at port : ${port}`)
});

